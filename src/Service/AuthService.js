import axios from "axios";
import {
    LOGIN_API_URL,
    CHANGE_PASSWORD_API_URL,
    UPDATE_PROFILE_API_URL,
    GET_USER_PROFILE,
    REGISTER_API_URL,
    GET_ALL_ORDERS
} from "../helper/Constant";

export default class AuthService {

    static login(data) {
        return axios.post(LOGIN_API_URL, data);
    }

    static register(data) {
        return axios.post(REGISTER_API_URL, data);
    }

    static getProfile() {
        return axios.get(GET_USER_PROFILE, AuthService.getAuthHeader());
    }

    static updateProfile(profile, id) {
        return axios.put(`${UPDATE_PROFILE_API_URL}/${id}`, profile, AuthService.getAuthHeader());
    }

    static changePassword(data, id) {
        return axios.put(`${CHANGE_PASSWORD_API_URL}/${id}`, data, AuthService.getAuthHeader());
    }

    static getAllOrders() {
        return axios.get(GET_ALL_ORDERS, AuthService.getAuthHeader());
    }

    static getAuthHeader = () => {
        const accessToken = localStorage.getItem("aiub-access-token");
        const options = {headers: {"x-auth-token": `${accessToken}`}}
        return options;
    }

}