import axios from "axios";
import {
    GET_ALL_ORDERS,
    CREATE_ORDER
} from "../helper/Constant";

export default class OrderService {

    static getAllOrders() {
        return axios.get(GET_ALL_ORDERS, OrderService.getAuthHeader());
    }

    static placeOrder(data) {
        return axios.post(CREATE_ORDER, data, OrderService.getAuthHeader());
    }

    static getAuthHeader = () => {
        const accessToken = localStorage.getItem("aiub-access-token");
        const options = {headers: {"x-auth-token": `${accessToken}`}}
        return options;
    }

}