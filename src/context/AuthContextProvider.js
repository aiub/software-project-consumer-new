import React, {createContext, useState} from 'react';
import { useToasts } from "react-toast-notifications";
import AuthService from '../Service/AuthService';

export const AuthContext = createContext();


const AuthContextProvider = ({children}) => {

    const { addToast } = useToasts();
    const token = localStorage.getItem('vpn-access-token');
    const [profile, setProfile] = useState(null);
    const [loadingLogin, setLoadingLogin] = useState(false);
    const [loadingProfile, setLoadingProfile] = useState(true);
    const [profileErrorMsg, setProfileErrorMsg] = useState('');

    const [isLogin, setIsLogin] = useState(!!token);

    const login = async (credential) => {
        try {
            setLoadingLogin(true);
            const response = await AuthService.login(credential);
            localStorage.setItem('aiub-access-token', response.data.token);
            setProfile(response.data.user);
            setIsLogin(true);
            setLoadingLogin(false);

        } catch (error) {
            setLoadingLogin(false);
            console.log(error.response);
            const message = error.response ? error.response.data : error.message;
            addToast(message, { appearance: "error", autoDismiss: true });
        }

    }

    const register = async (user) => {
        try {
            setLoadingLogin(true);
            const response = await AuthService.register({...user, userType: 'CUSTOMER'});
            setProfile(response.data.user);
            localStorage.setItem('aiub-access-token', response.data.token);
            setIsLogin(true);
            setLoadingLogin(false);

        } catch (error) {
            setLoadingLogin(false);
            console.log(error.response);
            const message = error.response ? error.response.data : error.message;
            addToast(message, { appearance: "error", autoDismiss: true });
        }

    }

    const getProfile = async () => {
        try {
            setLoadingProfile(true);
            let _token = localStorage.getItem('aiub-access-token');
            if (_token) {
                const response = await AuthService.getProfile();
                setIsLogin(true);
                setProfile(response.data);
            }else{
                setIsLogin(false);
            }
            setLoadingProfile(false);

        } catch (error) {
            logout();
            setLoadingProfile(false);
            const message = error.response ? error.response.data : error.message;
            setProfileErrorMsg(message);
            // ErrorModal(error);
        }
    }

    const updateProfile = async (data) => {
        try {
            setLoadingProfile(true);
            const response = await AuthService.updateProfile({fullName: data.fullName, phone: data.phone, email: data.email, deliveryAddress: data.deliveryAddress}, profile._id);
            setProfile(response.data);
            addToast('Profile updated successfully !', { appearance: "success", autoDismiss: true });
            setLoadingProfile(false);

        } catch (error) {
            setLoadingProfile(false);
            const message = error.response ? error.response.data : error.message;
            addToast(message, { appearance: "error", autoDismiss: true });
        }
    }

    const changePassword = async (password) => {
        if(!password) return;
        try {
            setLoadingProfile(true);
            const response = await AuthService.changePassword({newPassword: password}, profile._id);
            addToast('Password changed successfully !', { appearance: "success", autoDismiss: true });
            setLoadingProfile(false);

        } catch (error) {
            setLoadingProfile(false);
            const message = error.response ? error.response.data : error.message;
            addToast(message, { appearance: "error", autoDismiss: true });
        }
    }

    const logout = () => {
        localStorage.removeItem('aiub-access-token');
        setProfile(null);
        setIsLogin(false);
    }

    return (
        <AuthContext.Provider
            value={{
                isLogin,
                login,
                logout,
                register,
                profile,
                getProfile,
                loadingLogin,
                loadingProfile,
                profileErrorMsg,
                updateProfile,
                changePassword
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

export default AuthContextProvider;