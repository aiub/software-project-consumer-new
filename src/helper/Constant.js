export const ROOT_URL = "http://localhost:3002";
export const VERSION = "v1";
export const API_URL = `${ROOT_URL}/api/${VERSION}`;

//login API
export const LOGIN_API_URL = `${API_URL}/login/`;
export const CHANGE_PASSWORD_API_URL = `${API_URL}/user/change-password`;
export const UPDATE_PROFILE_API_URL = `${API_URL}/user/update`;
export const GET_USER_PROFILE = `${API_URL}/user/profile`;
export const REGISTER_API_URL = `${API_URL}/user/signup`

//order API
export const GET_ALL_ORDERS = `${API_URL}/orders/`;
export const CREATE_ORDER = `${API_URL}/orders/post`;