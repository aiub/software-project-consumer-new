export const ROOT_URL = "http://localhost:3002";
export const VERSION = "v1";
export const API_URL = `${ROOT_URL}/api/${VERSION}`;

//product apis
export const PRODUCT_API_URL = `${API_URL}/products/public`