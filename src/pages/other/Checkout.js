import PropTypes from "prop-types";
import React, { Fragment, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import MetaTags from "react-meta-tags";
import { connect } from "react-redux";
import { BreadcrumbsItem } from "react-breadcrumbs-dynamic";
import { getDiscountPrice } from "../../helpers/product";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import { AuthContext } from "../../context/AuthContextProvider";
import LoginRegister from "./LoginRegister";
import OrderService from "../../Service/OrderService";
import { useToasts } from "react-toast-notifications";

const Checkout = ({ location, cartItems, currency }) => {

  const authContext = useContext(AuthContext);
  const [orderNote, setOrderNote] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [loadingPlaceOrder, setLoadingPlaceOrder] = useState(false);
  const { addToast } = useToasts();

  const { pathname } = location;
  let cartTotalPrice = 0;

  useEffect(()=> {
    if(authContext.profile && authContext.profile.deliveryAddress)
      setDeliveryAddress(authContext.profile.deliveryAddress);
  }, [authContext.profile])

  const onPlaceOrder = async () => {
    try{
      if(!deliveryAddress){
        addToast('Please input your delivery address', { appearance: "warning", autoDismiss: true });
        return
      }
      setLoadingPlaceOrder(true);
      const order = {
        itemList: [...cartItems.map(
            e => ({
              productId: e.id,
              color: e.selectedProductColor,
              size: e.selectedProductSize,
              quantity: e.quantity
            })
          )],
          deliveryAddress: deliveryAddress,
          orderNote: orderNote
        };
      const response = await OrderService.placeOrder(order);
      setLoadingPlaceOrder(false);
      addToast('Ordered placed successfully.', { appearance: "success", autoDismiss: true });
    }catch(error){
      setLoadingPlaceOrder(false);
      const message = error.response ? error.response.data : error.message;
      addToast(message, { appearance: "error", autoDismiss: true });
    }
  }

  return (
    <Fragment>
      <MetaTags>
        <title>UBCom. | Checkout</title>
        <meta
          name="description"
          content="Checkout page of UBCom react minimalist eCommerce template."
        />
      </MetaTags>
      <BreadcrumbsItem to={process.env.PUBLIC_URL + "/"}>Home</BreadcrumbsItem>
      <BreadcrumbsItem to={process.env.PUBLIC_URL + pathname}>
        Checkout
      </BreadcrumbsItem>
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb />
        {
          !authContext.isLogin ? <LoginRegister notAPage = {true}/> :
          <div className="checkout-area pt-95 pb-100">
          <div className="container">
            {cartItems && cartItems.length >= 1 ? (
              <div className="row">
                <div className="col-lg-7">
                  <div className="billing-info-wrap">
                    <h3>Billing Details</h3>
                    <div className="row">
                      
                      <div className="col-lg-12">
                        <div className="billing-info mb-20">
                          <label>Delivery Address</label>
                          <input
                            className="billing-address"
                            placeholder="House number and street name"
                            type="text"
                            value = {deliveryAddress}
                            onChange = {e => {
                              setDeliveryAddress(e.target.value)
                            }}
                          />
                        </div>
                      </div>
                      
                    </div>

                    <div className="additional-info-wrap">
                      <h4>Additional information</h4>
                      <div className="additional-info">
                        <label>Order notes</label>
                        <textarea
                          placeholder="Notes about your order, e.g. special notes for delivery. "
                          name="message"
                          defaultValue={""}
                          value= {orderNote}
                          onChange = { e => {
                            setOrderNote(e.target.value)
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-5">
                  <div className="your-order-area">
                    <h3>Your order</h3>
                    <div className="your-order-wrap gray-bg-4">
                      <div className="your-order-product-info">
                        <div className="your-order-top">
                          <ul>
                            <li>Product</li>
                            <li>Total</li>
                          </ul>
                        </div>
                        <div className="your-order-middle">
                          <ul>
                            {cartItems.map((cartItem, key) => {
                              const discountedPrice = getDiscountPrice(
                                cartItem.price,
                                cartItem.discount
                              );
                              const finalProductPrice = (
                                cartItem.price * currency.currencyRate
                              ).toFixed(2);
                              const finalDiscountedPrice = (
                                discountedPrice * currency.currencyRate
                              ).toFixed(2);

                              discountedPrice != null
                                ? (cartTotalPrice +=
                                    finalDiscountedPrice * cartItem.quantity)
                                : (cartTotalPrice +=
                                    finalProductPrice * cartItem.quantity);
                              return (
                                <li key={key}>
                                  <span className="order-middle-left">
                                    {cartItem.name} X {cartItem.quantity}
                                  </span>{" "}
                                  <span className="order-price">
                                    {discountedPrice !== null
                                      ? currency.currencySymbol +
                                        (
                                          finalDiscountedPrice *
                                          cartItem.quantity
                                        ).toFixed(2)
                                      : currency.currencySymbol +
                                        (
                                          finalProductPrice * cartItem.quantity
                                        ).toFixed(2)}
                                  </span>
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                        <div className="your-order-bottom">
                          <ul>
                            <li className="your-order-shipping">Shipping</li>
                            <li>Free shipping</li>
                          </ul>
                        </div>
                        <div className="your-order-total">
                          <ul>
                            <li className="order-total">Total</li>
                            <li>
                              {currency.currencySymbol +
                                cartTotalPrice.toFixed(2)}
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="payment-method"></div>
                    </div>
                    <div className="place-order mt-25">
                      {
                        loadingPlaceOrder ? <p>Loading ...</p> :
                        <button className="btn-hover" onClick = {onPlaceOrder}>Place Order</button>
                      }
                      
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="row">
                <div className="col-lg-12">
                  <div className="item-empty-area text-center">
                    <div className="item-empty-area__icon mb-30">
                      <i className="pe-7s-cash"></i>
                    </div>
                    <div className="item-empty-area__text">
                      No items found in cart to checkout <br />{" "}
                      <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                        Shop Now
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        }
        
      </LayoutOne>
    </Fragment>
  );
};

Checkout.propTypes = {
  cartItems: PropTypes.array,
  currency: PropTypes.object,
  location: PropTypes.object
};

const mapStateToProps = state => {
  return {
    cartItems: state.cartData,
    currency: state.currencyData
  };
};

export default connect(mapStateToProps)(Checkout);
