import PropTypes from "prop-types";
import React, { Fragment, useContext, useState } from "react";
import MetaTags from "react-meta-tags";
import { Link, Redirect } from "react-router-dom";
import { BreadcrumbsItem } from "react-breadcrumbs-dynamic";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import { AuthContext } from "../../context/AuthContextProvider";

const LoginRegister = (props) => {
  const  pathname  = props.location ? props.location.pathName : '';

  const authContext = useContext(AuthContext);
  const [user, setUser] = useState({
    email: '',
    password: '',
    fullName: ''
  });

  const handleInputChange = (e) => {
    setUser({...user, [e.target.name] : e.target.value});
    console.log(e.target.name, user);
  }

  const onLogin = (e) => {
    e.preventDefault();
    authContext.login({email: user.email, password: user.password})
  }

  const onRegister = (e) => {
    e.preventDefault();
    authContext.register(user);
  }

  return (
    <Fragment>
      { !props.notAPage && <MetaTags>
        <title>UBCom. | Login</title>
        <meta
          name="description"
          content="Compare page of flone react minimalist eCommerce template."
        />
      </MetaTags>}
      {
        authContext.isLogin && !props.notAPage && <Redirect to = {process.env.PUBLIC_URL + "/my-account"}/>
      }
      { !props.notAPage && 
        <>
          <BreadcrumbsItem to={process.env.PUBLIC_URL + "/"}>Home</BreadcrumbsItem>
          <BreadcrumbsItem to={process.env.PUBLIC_URL + pathname}>
          
            Login Register
          </BreadcrumbsItem>
        </>
      }
      <LayoutOne headerTop={"visible"} showHeader = {!props.notAPage} showFooter = {!props.notAPage}>
        {/* breadcrumb */}
        { !props.notAPage && <Breadcrumb />}
        <div className="login-register-area pt-100 pb-100">
          <div className="container">
            <div className="row">
              <div className="col-lg-7 col-md-12 ml-auto mr-auto">
                <div className="login-register-wrapper">
                  <Tab.Container defaultActiveKey="login">
                    <Nav variant="pills" className="login-register-tab-list">
                      <Nav.Item>
                        <Nav.Link eventKey="login">
                          <h4>Login</h4>
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="register">
                          <h4>Register</h4>
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content>
                      <Tab.Pane eventKey="login">
                        <div className="login-form-container">
                          <div className="login-register-form">
                            <form>
                              <input
                                type="text"
                                name="email"
                                placeholder="Email"
                                onChange= {handleInputChange}
                              />
                              <input
                                type="password"
                                name="password"
                                placeholder="Password"
                                onChange= {handleInputChange}
                              />
                              <div className="button-box">
                                {/* <div className="login-toggle-btn">
                                  <input type="checkbox" />
                                  <label className="ml-10">Remember me</label>
                                  <Link to={process.env.PUBLIC_URL + "/"}>
                                    Forgot Password?
                                  </Link>
                                </div> */}
                                <button onClick = {onLogin}>
                                  <span>Login</span>
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey="register">
                        <div className="login-form-container">
                          <div className="login-register-form">
                            <form>
                            <input
                                name="fullName"
                                placeholder="Full Name"
                                type="text"
                                onChange= {handleInputChange}
                              />
                              <input
                                type="text"
                                name="email"
                                placeholder="Email"
                                onChange= {handleInputChange}
                              />
                              <input
                                type="password"
                                name="password"
                                placeholder="Password"
                                onChange= {handleInputChange}
                              />
                             
                              <div className="button-box">
                                <button onClick = {onRegister}>
                                  <span>Register</span>
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

LoginRegister.propTypes = {
  location: PropTypes.object
};

export default LoginRegister;
