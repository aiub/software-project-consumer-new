import PropTypes from "prop-types";
import React, { Fragment, useContext, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import { BreadcrumbsItem } from "react-breadcrumbs-dynamic";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import { AuthContext } from "../../context/AuthContextProvider";
import { Redirect } from "react-router-dom";
import {Table} from 'antd';
import LoadingSuspense from "../../components/common/LoadingSuspense";
import { useToasts } from "react-toast-notifications";
import AuthService from "../../Service/AuthService";
import { ROOT_URL } from "../../helper/Constant";



const Orders = ({ location }) => {
  const authContext = useContext(AuthContext);
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);
  const { pathname } = location;
  const { addToast } = useToasts();

  useEffect(() => {
    if(authContext.isLogin){
        getOrder();
    }
  }, [authContext.isLogin])

  const getOrder = async () => {
    try{
        setLoading(true);
        const response = await AuthService.getAllOrders();
        setOrders(response.data);
        setLoading(false);
    }catch(error){
        console.log(error.response);
        const message = error.response ? error.response.data : error.message;
        addToast(message, { appearance: "error", autoDismiss: true });
    }
  }

  return (
    <Fragment>
      <MetaTags>
        <title>UBCom. | My Orders</title>
        <meta
          name="description"
          content="Compare page of UBCom react minimalist eCommerce template."
        />
      </MetaTags>
      
      <BreadcrumbsItem to={process.env.PUBLIC_URL + "/"}>Home</BreadcrumbsItem>
      <BreadcrumbsItem to={process.env.PUBLIC_URL + pathname}>
        My Orders
      </BreadcrumbsItem>
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb />
        <div className="myaccount-area pb-80 pt-100">
          <div className="container">
            {
                loading ? <LoadingSuspense/> :
                !authContext.isLogin ? <Redirect to = {process.env.PUBLIC_URL + ""}/> :
                <Table
                    columns={columns}
                    expandable={{
                        expandedRowRender: record => {
                            return (
                                <div style ={{marginLeft: '50px'}}>
                                    <h4
                                        style={{ color : record.status === 'Order has been canceled' ? 'brown' :
                                        record.status=== 'Order has been delivered' ? 'green' :
                                        'blue'
                                    }}
                                        >{record.status}</h4>
                                    <h4>Items: </h4>
                                    <Table
                                        columns = {variantColumns}
                                        dataSource = {record.orderedProducts.map((e, i) => ({ ...e, key: i }))}
                                        pagination = {false}/>

                                    <br/>
                                    <h4>Total discount</h4>
                                    <p>{record.totalDiscount}</p><br/>
                                    <h4>Delivery address</h4>
                                    <p>{record.deliveryAddress}</p><br/>
                                    { record.orderNote &&
                                        <>
                                            <h4>Order note</h4>
                                            <p>{record.orderNote}</p>
                                        </>
                                    }
                                </div>
                            )
                        },
                        rowExpandable: record => record.name !== 'Not Expandable',
                      }}
                    dataSource={orders.map((e, i) => ({ ...e, key: i }))}
                />
            }
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

const columns = [
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        width: '20%',
        render: e => (<p
            style={{ color : e === 'Order has been canceled' ? 'brown' :
            e=== 'Order has been delivered' ? 'green' :
            'blue'
        }}
            >{e}</p>)
    },
    {
        title: "Date",
        dataIndex: "createdAt",
        key: "createdAt",
        width: "20%",
        render: e => new Date(e).toLocaleString('en-US', { hour12: true })
    },
    {
        title: 'Total Cost',
        dataIndex: 'totalCost',
        key: 'totalCost',
        width: '20%',
    },
    {
        title: 'Actual cost',
        dataIndex: 'actualCost',
        key: 'actualCost',
        width: '20%',
    }
];

const variantColumns = [
    {
        title: 'Image',
        dataIndex: 'image',
        key: 'image',
        width: '20%',
        render: e => <img src={`${ROOT_URL}${e}`} style={{ height: "60px", width: "60px", borderRadius: "5px" }} alt="no content" />
    },
    {
        title: "Size",
        dataIndex: "size",
        key: "size",
        width: "20%",
    },
    {
        title: 'Color',
        dataIndex: 'color',
        key: 'color',
        width: '20%',
    },
    {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity',
        width: '20%',
    }
];

Orders.propTypes = {
  location: PropTypes.object
};

export default Orders;
