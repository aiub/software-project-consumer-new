import axios from "axios";
import { PRODUCT_API_URL, ROOT_URL } from "../../helpers/constant";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";


const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: products
});

// fetch products
export const fetchProducts = (params) => {
  return async dispatch => {
    const res = await axios.get(PRODUCT_API_URL, {params});
    const products = res.data.map( p => {

      let variants = [...p.productVariant.variants];
      let colorList = variants.map( e => e.attrs.colorFamily);
      colorList = [...new Set(colorList)];

      let variantList = [];

      for(let j=0; j<variants.length; j++){
        for(let i= 0; i< colorList.length; i++){
          if(variants[j].attrs.colorFamily == colorList[i]){
            variantList.push({color: colorList[i], image: variants[j].images[0], size: []});
            colorList = [...colorList.filter(c => c != colorList[i])];
          }
        }
      }

      for(let i=0;i<variantList.length; i++){
        for(let j=0; j<variants.length;j++){
          if(variantList[i].color == variants[j].attrs.colorFamily){
            variantList[i].size.push({
              name: variants[j].attrs.size,
              stock: variants[j].quantity
            })
          }
        }
      }

      return {
        id: p._id,
        sku: p._id,
        name: p.name,
        discount: p.discountPercentage,
        price: p.price,
        new: p.new,
        totalStock: p.totalStock,
        variation: variantList,
        rating: 4,
        category: p.category,
        tag: p.tags,
        image: p.thumbnailImages.map( img =>`${ROOT_URL}${img}`),
        shortDescription: p.shortDetails,
        fullDescription: p.description.description
      }
    });
    
    dispatch(fetchProductsSuccess(products));
  };
};
